import { expect, tap } from '@pushrocks/tapbundle';
import * as smartpromise from '../ts/index.js';

tap.test('should return a Deferred for .defer()', async () => {
  const myDeferred = smartpromise.defer();
  myDeferred.resolve();
  await myDeferred.promise;
});

tap.test('should let types flow through the Promise', async () => {
  const myString = 'someString';
  const myDeferred = smartpromise.defer<string>();
  myDeferred.resolve(myString);
  const result = await myDeferred.promise;
  expect(result).toEqual('someString');
});

tap.test('should map callbacks', async () => {
  const inputArray = ['hi', 'awesome'];
  const myPromisified = async (myInput) => {
    return myInput;
  };
  const result = await smartpromise.map(inputArray, myPromisified);
  expect(result).toEqual(inputArray);
});

tap.start();
