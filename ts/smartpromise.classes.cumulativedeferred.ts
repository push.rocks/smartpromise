import { defer } from "./smartpromise.classes.deferred.js";

export class CumulativeDeferred {
  private accumulatedPromises: Promise<any>[] = [];
  private deferred = defer();
  public promise = this.deferred.promise;

  constructor() {
    setTimeout(async () => {
      while (this.accumulatedPromises.length > 0) {
        const poppedPromise = this.accumulatedPromises.shift();
        await poppedPromise;
      }
      this.deferred.resolve();
    }, 0);
  }

  public addPromise(promiseArg: Promise<any>) {
    this.accumulatedPromises.push(promiseArg);
  }
}


export const cumulativeDefer = () => {
  return new CumulativeDeferred();
}